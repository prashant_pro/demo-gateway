package in.prashantpro.springfirstapi;

import org.springframework.http.HttpHeaders;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * HelloController
 */
@RestController
@RequestMapping("/users")
public class HelloController {

    @GetMapping("/")
    public String echo(ServletServerHttpRequest request, ServletServerHttpResponse response) {
        HttpHeaders headers = request.getHeaders();
        headers.forEach((k, v) -> {
            System.out.println("k =" + k + ", v =" + v);
        });
        return "Hello Spring API Called";
    }

    @GetMapping("/message")
    public String test() {
        return "Hello Spring API message";
    }

}